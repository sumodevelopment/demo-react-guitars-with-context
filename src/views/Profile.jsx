
import { Link } from "react-router-dom"
import { useGuitars } from "../context/GuitarsProvider"
import { useUser } from "../context/UserProvider"

const Profile = () => {
    const { guitars } = useGuitars()
    const {user} = useUser()

    // SHow my favourite guitars.
    const favouriteGuitars = guitars.filter(g => g.favourite)
    
    return (
        <>
        <Link to="/guitars">Guitars</Link>
            <h1>Welcome { user }</h1>
            { favouriteGuitars.map(g => <li key={g.id}>{ g.model }</li>) }
        </>
    )
}
export default Profile