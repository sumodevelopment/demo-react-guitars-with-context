import { Link } from "react-router-dom"
import GuitarList from "../components/GuitarList"

const Guitars = () => {
    return (
        <>
            <Link to="/profile">Profile</Link>
            <h1>guitars</h1>
            <GuitarList />
        </>
    )
}
export default Guitars