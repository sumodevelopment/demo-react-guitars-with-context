const GuitarListItem = ({ guitar, onGuitarClick }) => {

    return (
        <li onClick={ () => onGuitarClick(guitar) }>
            <img src={ guitar.image } alt="Some guitar" width="100" />
            <span>{ guitar.model }</span>
            <span>{ guitar.manufacturer }</span>
            { guitar.favourite && <p>Favourite</p> }
        </li>
    )
}

export default GuitarListItem