import { useEffect, useState } from "react";
import { apiFetchAllGuitars } from "../api/guitars";
import {  useGuitars } from "../context/GuitarsProvider";
import GuitarListItem from "./GuitarListItem";

const GuitarList = () => {
  const { guitars, favouriteGuitar } = useGuitars()
  const [loading, setLoading] = useState(true);

  const onGuitarClick = (guitar) => {
    favouriteGuitar(guitar.id)
    console.log("Executed in PARENT", guitar.model);
  }

  const totalFavourite = guitars.filter(g => g.favourite).length

  return (
    <>
      {loading && <p>Loading guitars...</p>}
      <p>You have { totalFavourite } favourites</p>
      <ul>
        {guitars.map(guitar => 
          <GuitarListItem 
            key={guitar.id} 
            guitar={ guitar } 
            onGuitarClick={ onGuitarClick } />) 
        }
      </ul>
    </>
  );
};

export default GuitarList;
