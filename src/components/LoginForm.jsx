import { useEffect, useState } from "react"; // Hook
import { useNavigate } from "react-router-dom";
import { useUser } from "../context/UserProvider";

const LoginForm = () => {
  // Array
  // 0: Value
  // 1: Dispatch
  const {user, setUser} = useUser()


  const navigate = useNavigate()

  console.log("LoginForm.render()", user);

  // Arg 1: Callback
  // Arg 2: List of deps
  /**useEffect(() => {
    setUsername("Hvem som helst");
  }, []); // Hook
*/
  const handleUsernameChange = (event) => {
    // SyntheticEvent
    // Target
    setUser(event.target.value.trim());
  };

  const handleLoginClick = () => {
    // Do something inter.
    // Navigate!
    navigate("/guitars")
  };

  const items = [1, 2, 3, 4, 5];

  const handleItemClick = (item) => {
    console.log(item);
  };

  // Return valid JSX
  return (
    <>
      <input type="text" id="username" onChange={handleUsernameChange} />
      <p>{user}</p>

  
      <footer>
        <button onClick={handleLoginClick}>Login</button>
      </footer>

      <br />

      <section>
      {items.map((item) => (
        <button key={item} onClick={() => handleItemClick(item)}>
          {item}
        </button>
      ))}
      </section>
    </>
  );
};

export default LoginForm;
