import { createContext, useContext, useState } from "react"

const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext)
}

const UserProvider = (props) => {
    const [user, setUser] = useState()
    return (
        <UserContext.Provider value={ { user, setUser } }>
            { props.children }
        </UserContext.Provider>
    )
}
export default UserProvider