export function apiFetchAllGuitars() { // Promise 
   return fetch("https://dce-noroff-api.herokuapp.com/guitars") 
        // Promise - EventLoop
      .then((response) => response.json()) // BODY
}